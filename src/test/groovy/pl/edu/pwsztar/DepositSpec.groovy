package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification {

    static BankOperation bank;

    def setupSpec() {
        bank = new Bank();
    }

    @Unroll
    def "should deposit #amount to #accountNumber"() {

        given:

        def accountBalance = bank.accountBalance(accountNumber)

        when: "money is being deposited on #account"
        def operationResult = bank.deposit(accountNumber,amount)
        then: "check account balance"
        accountBalance < 0 && operationResult && accountBalance == (amount - bank.accountBalance(accountNumber))

        where:
        amount   | accountNumber
        100 | 1
        5000  | 2
        -100 | 3
    }

}
