package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification {
    static BankOperation bank;

    def setupSpec() {
        bank = new Bank();
    }

    @Unroll
    def "should transfer from #fromAccountNumber to #toAccountNumber"() {
        given:
        bank.deposit(1, 5400);

        when: "money is being transfered from #fromAccountNumber to #toAccountNumber"
        def result = bank.transfer(fromAccountNumber,toAccountNumber, amount)
        then: "check account number"
        result

        where:
        fromAccountNumber   | toAccountNumber | amount
        -1                  |       997       |    100
        1                   |         2       |    -10
        1                   |         2       |    500
        1                   |         2       |   5000
    }

}
