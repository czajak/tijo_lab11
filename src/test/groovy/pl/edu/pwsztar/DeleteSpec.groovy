package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteSpec extends Specification {

    static BankOperation bank;

    def setupSpec() {
        bank = new Bank();
    }

    @Unroll
    def "should delete #accountNumber"() {
        given:
            int acc1 = bank.createAccount()
            int acc2 = bank.createAccount()

        when: "account is being deleted"
        def result = bank.deleteAccount(accountNumber)
        then: "check account number"
        result == 0

        where:
        expected   | accountNumber
        true       |             1
        true       |             2
        false      |             -1
    }
}