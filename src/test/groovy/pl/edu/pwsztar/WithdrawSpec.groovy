package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification {

    static BankOperation bank;

    def setupSpec() {
        bank = new Bank();
    }

    @Unroll
    def "should withdraw #amount to #accountNumber"() {

        given:

        def accountBalance = bank.accountBalance(accountNumber)

        when: "account is being withdraw from"
        def operationResult = bank.withdraw(accountNumber,amount)
        then: "check account balance"
        accountBalance > 0 && operationResult && accountBalance == (amount + bank.accountBalance(accountNumber))

        where:
        amount   | accountNumber
        100 | 1
        5000  | 2
        -100 | 3
    }
}
