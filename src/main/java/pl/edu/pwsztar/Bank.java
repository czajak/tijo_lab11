package pl.edu.pwsztar;

import java.util.LinkedList;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)
class Bank implements BankOperation {

    private LinkedList<Account> accounts;

    private int accountNumber = 0;

    public int createAccount() {
        Account newAccount = new Account();
        accounts.push(newAccount);
        return ++accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        if(doesAccountExists(accountNumber)){
            accounts.remove(accountNumber);
        }
        return ACCOUNT_NOT_EXISTS;
    }

    public boolean deposit(int accountNumber, int amount) {
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        return false;
    }

    public int accountBalance(int accountNumber) {
        if(!doesAccountExists(accountNumber)){
            return -1;
        }
        return getAccountFromList(accountNumber).getAccountBalance();
    }

    public int sumAccountsBalance() {
        return 0;
    }

    private boolean doesAccountExists(int id){
        for (Account a: accounts) {
            if(a.getAccountNumber() == id){
                return true;
            }
        }
        return false;
    }

    private Account getAccountFromList(int id) {
        for (Account a : accounts) {
            if (a.getAccountNumber() == id) {
                return a;
            }

        }
        return null;
    }
}
